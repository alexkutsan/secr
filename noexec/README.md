0. Compile executable

 $ g++ main.cc

1. Create file with ext4 file system

 $ fallocate -l 10M ext4file
 $ mkfs -t ext4 ext4file

2. Mount file to directory

 $ mkdir mnt
 $ mount ext4file ./mnt

3. Copy executable in dir and run it

  $ sudo cp ./a.out ./mnt/
  $ ./mnt/a.out

4. Unmunt dir 
 $ sudo umount mn

5. mount file system with “-o noexec” parameter

 $ sudo mount -o noexec ./ext4file mnt/

6. Run existing executable

$ ./mnt/a.out
bash: ./mnt/a.out: Permission denied
