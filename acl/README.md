0. Create ext4 fs file 

 $ fallocate -l 10M ext4file
 $ mkfs -t ext4 ext4file

1. Mount file system with “-o acl” parameter, give yuour user ownership of mounted dir

 $ sudo mount -o acl” ./ext4file mnt/
 $ chown $USER mnt -R


2. Create file with 660 access in mnt
 
 $ cd mnt 
 $ touch file

3. Create new user 
 
 $ useradd testuser


4. Login as new user
 
 $ sudo su testuser

5. Try to read file, write to file (Permission denied)
 $ cat file
 $ echo "DDD" > file

6. Go back as original user: $ exit

7. With ACL provide ability for “test user” to write to the file
 
 $ setfacl -m u:testuser:rw ./file

8. Perform 4,5 again
9. Try with another user - testuser2, give only read and only write access
