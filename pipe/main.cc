#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int main() {
    const char* buf  = "Hello world";
    mkfifo("fifo_x", 0666);
    int fd = open("fifo_x", O_WRONLY);
    write(fd, buf, strlen(buf));
    return 0;
}

