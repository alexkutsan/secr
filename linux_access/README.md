1. COmpile programm:

```
g++ main.cc suid_check
```

2. 
`./suid_check r` will read file from /root dir
`./suid_check w` will write file to /root dir

User cant read and write such files test it with this commands.


3. Change owner of the programm to root

`sudo chown root ./suid_check`

Check that executing of programm stil permission denied

4. Give suid bit to the programm

`sudo chmod u+s ./suid_check`

Check that programm without root password can read and write files in root dir
