#include <fstream>
#include <iostream>
#include <string.h>

using std::fstream;

int main(int argc, char *argv[]) {
    fstream file;
    if (strcmp(argv[1] , "r") == 0) {
      file.open("/root/test.txt",fstream::in);
      std::cerr << "strerror: " << strerror(errno) << std::endl;
      std::string content;
      file >> content;
      std::cout << content << std::endl;
    }
    if (strcmp(argv[1], "w") == 0) {
      file.open("/root/test.txt",fstream::out);
      std::cerr << "strerror: " << strerror(errno) << std::endl;
      file << "test";
    }
    file.close();
}
