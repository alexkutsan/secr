#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
int main () {
  std::cout << getpid() << std::endl;
  pid_t child_pid = fork();
  if (child_pid > 0) sleep (20); // parent process
  else sleep(10); // child process
  return 0;
}

