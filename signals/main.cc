#include <string.h>
#include<signal.h>
#include<unistd.h>
#include<iostream>

void sig_handler(int signo) {
    auto sig_str = strsignal(signo);
    std::cout << "received " << sig_str << " " << getpid() << std::endl;
}

int main(void) {
 std::cout << "Hello from " << getpid() << std::endl;
 if (signal(SIGUSR1, sig_handler) == SIG_ERR)
     std::cout << "Can't catch SIGUSR1" << std::endl;
 if (signal(SIGINT, sig_handler) == SIG_ERR)
     std::cout << "Can't catch SIGINT" << std::endl;
 if (signal(SIGKILL, sig_handler) == SIG_ERR)
     std::cout << "Can't catch SIGKILL" << std::endl;
 if (signal(SIGSTOP, sig_handler) == SIG_ERR)
     std::cout << "Can't catch SIGSTOP" << std::endl;
 
 while(true) {
     sleep(10);
     printf("Sleep cycle\n");
  }
 return 0;
}

