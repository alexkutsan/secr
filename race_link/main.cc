#include <fstream>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h> 

using std::fstream;

int main(int argc, char *argv[]) {
	const char* fname = "/tmp/temporary.txt";

	if( access( fname, F_OK ) != -1 ) {
		std::cout << "Remove file" << fname << std::endl;
		remove(fname);
	}
	sleep(10);
	fstream file;
	std::cout << "Openfile " << fname << std::endl;
	file.open(fname, fstream::out);
	file << "data\n";
	file.close();
}
